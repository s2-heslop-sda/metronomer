#include "Metronome.h"

// --------------------------
// Public Methods
// --------------------------
Metronome::Metronome(String audioFileName) : audioFilePlayer(audioFileName)
{
	denominator = 1;
	bpm = 50;
	amplitude = 0.5f;
	state = State::Stopped;

	padding = 10;
	buttonDimension = 20;
	xStartingPosition = 10;
	yStartingPosition = 10;

	initialiseMetronomeNameLabel(audioFileName);
	initialiseDenominatorLabel();
	initialiseVolumeSlider();
	initialiseButton(&playButton, "play", 2);
	initialiseButton(&pauseButton, "pause", 3);
}

Metronome::~Metronome(void)
{
}

// ----- Metronome logic -----
void Metronome::setBpm(int newBpm)
{
	bpm = newBpm;
	stopTimer();

	if (state == State::Playing)
		startTimer(metronomeInterval());
}

void Metronome::syncTimerStart()
{
	if (state == State::Syncing)
	{
		audioFilePlayer.play();
		state = State::Playing;
		startTimer(metronomeInterval());
	}
}

// --------------------------
// Private Methods
// --------------------------

// ----- Metronome logic -----
void Metronome::setDenominator()
{
	denominator = atoi((denominatorLabel.getText().getCharPointer()));
}

int Metronome::metronomeInterval()
{
	return ((60000/bpm) / denominator);
}

void Metronome::timerCallback(void)
{
	if (state == State::Playing)
		audioFilePlayer.play();
}

// ----- UI Initialisation -----
void Metronome::initialiseMetronomeNameLabel(String audioFileName)
{
	metronomeNameLabel.setSize(100, buttonDimension);
	metronomeNameLabel.setTopLeftPosition(5, yStartingPosition);
	metronomeNameLabel.setEditable(false);

	String trimmedAudioFileName = audioFileName.trimCharactersAtEnd(".wav").replaceCharacter('_',' ');
	metronomeNameLabel.setText(trimmedAudioFileName, NotificationType::sendNotification);

	addAndMakeVisible(metronomeNameLabel);
}

void Metronome::initialiseDenominatorLabel()
{
	denominatorLabel.setSize(70, buttonDimension);
	denominatorLabel.setTopLeftPosition(5, yStartingPosition + 30);
	denominatorLabel.setEditable(true);

	denominatorLabel.setText(String(denominator), NotificationType::sendNotification);

	addAndMakeVisible(denominatorLabel);
	denominatorLabel.addListener(this);
}

void Metronome::initialiseButton(ImageButton* button, String buttonType, int positionInRow)
{
	File buttonImageFile = 
		File::getCurrentWorkingDirectory()
		.getChildFile(buttonType + ".png");

	Image buttonImage = PNGImageFormat::loadFrom(buttonImageFile);

	button->setImages(true, true, true,
                buttonImage, 0.7f, Colours::transparentBlack,
				buttonImage, 1.0f, Colours::transparentBlack,
                buttonImage, 1.0f, Colours::green.withAlpha(0.8f),
                0.5f);

	button->setSize(buttonDimension, buttonDimension);
	int verticalPosition = yStartingPosition + (positionInRow * buttonDimension) + (padding * positionInRow);
	button->setTopLeftPosition(xStartingPosition, verticalPosition );

	addAndMakeVisible(button);
	button->addListener(this);
}

void Metronome::initialiseVolumeSlider()
{
	volumeSlider.setSliderStyle(Slider::LinearVertical);
	volumeSlider.setBounds(6, 125, 30, 80);
	volumeSlider.setRange(0, 1, 0.1);
	volumeSlider.setValue(amplitude);
	volumeSlider.addListener(this);
	addAndMakeVisible(volumeSlider);
}

// ----- UI Listeners -----
void Metronome::buttonClicked(Button* clickedButton)
{
	if (clickedButton == &playButton)
	{
		state = State::Syncing;
		metronomeNameLabel.setColour(juce::Label::textColourId, Colours::green);
	}
		
	else if (clickedButton == &pauseButton)
	{
		stopTimer();
		state = State::Stopped;
		metronomeNameLabel.setColour(juce::Label::textColourId, Colours::black);
	}
}

void Metronome::labelTextChanged(Label *changedLabel)
{
	if(changedLabel == &denominatorLabel)
	{
		stopTimer();
		setDenominator();
		state = State::Syncing;
		metronomeNameLabel.setColour(juce::Label::textColourId, Colours::green);
	}
}

void Metronome::sliderValueChanged(Slider *slider)
{
	if (slider == &volumeSlider)
	{
		amplitude = slider->getValue();
		audioFilePlayer.setAmplitude(amplitude);
	}
}