#pragma once
#include "JuceHeader.h"

///
/// Simple Audio abstraction to allow for simple playback of a single audio file.
///
class AudioFilePlayer
{
public:
	AudioFilePlayer(String fileName);
	~AudioFilePlayer(void);

	// Audio File playback

	///
	/// Trigger the audio file to play once.
	///
	void play();

	///
	/// Controls the amplitude at which the audio file is played at.
	///
	void setAmplitude(float amplitude);

private:

	// Audio File playback
	AudioFormatManager *formatManager;
	ScopedPointer<AudioFormatReaderSource> readerSource;
	AudioTransportSource transportSource;
	AudioSourcePlayer player;
	AudioDeviceManager deviceManager;
};

