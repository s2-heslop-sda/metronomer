#include "AudioFilePlayer.h"

// --------------------------
// Public Methods
// --------------------------

AudioFilePlayer::AudioFilePlayer(String fileName)
{
	File audioFile = File::getCurrentWorkingDirectory()
							.getChildFile(fileName);
	
	formatManager = new AudioFormatManager();
	formatManager->registerBasicFormats();

	player.setSource(&transportSource);
	player.setGain(0.5);
	deviceManager.addAudioCallback(&player);
	deviceManager.initialise(0, 2, nullptr, true);

	readerSource = new AudioFormatReaderSource(
		formatManager->createReaderFor(audioFile),
		true);

	transportSource.setSource(readerSource);
}

AudioFilePlayer::~AudioFilePlayer(void)
{
	transportSource.stop();
	transportSource.setSource(nullptr);

	player.setSource(nullptr);
		
	deviceManager.removeAudioCallback(&player);
	deviceManager.closeAudioDevice();

	delete formatManager;
}

// ----- Audio file playback -----
void AudioFilePlayer::play(void)
{
	transportSource.start();

	player.setSource(&transportSource);
	transportSource.setNextReadPosition(0);
}

void AudioFilePlayer::setAmplitude(float amplitude)
{
	if(!(amplitude < 0.1 || amplitude > 0.9))
		player.setGain(amplitude);
}
