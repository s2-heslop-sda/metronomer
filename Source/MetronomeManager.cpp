#include "MetronomeManager.h"

// --------------------------
// Public Methods
// --------------------------

MetronomeManager::MetronomeManager(void) : Thread("MetronomeManager"),
											kickDrum("Kick.wav"),
											snareDrum("Snare.wav"),
											tomDrum("Tom.wav"),
											highHat("High_Hat.wav")
{
	bpm = 50;
	
	initialiseMetronomeUI(&kickDrum, 1);
	initialiseMetronomeUI(&snareDrum, 2);
	initialiseMetronomeUI(&tomDrum, 3);
	initialiseMetronomeUI(&highHat, 4);
	setAllMetronomeBpms();

	bpmSlider.setBounds(150, 350, 200, 30);
	bpmSlider.setRange(25, 100, 1);
	bpmSlider.setValue(bpm);
	addAndMakeVisible(&bpmSlider);
	bpmSlider.addListener(this);

	bpmSliderLabel.setText("BPM Control", dontSendNotification);
	bpmSliderLabel.setBounds(145, 320, 200, 30);
	addAndMakeVisible(bpmSliderLabel);

	startThread();
}

MetronomeManager::~MetronomeManager(void)
{
	stopThread(500);
}


// --------------------------
// Private Methods
// --------------------------

// ----- Metronome logic -----

void MetronomeManager::setAllMetronomeBpms()
{
	kickDrum.setBpm(bpm);
	snareDrum.setBpm(bpm);
	tomDrum.setBpm(bpm);
	highHat.setBpm(bpm);
}

void MetronomeManager::syncAllMetronomes()
{
	kickDrum.syncTimerStart();
	snareDrum.syncTimerStart();
	tomDrum.syncTimerStart();
	highHat.syncTimerStart();
}

void MetronomeManager::timerCallback()
{
	syncAllMetronomes();
}

// ----- Threading -----

void MetronomeManager::run()
{
	startTimer((60000/bpm));
}

// ----- UI -----

void MetronomeManager::initialiseMetronomeUI(Metronome* metronome, int positionRowCount)
{
	int metronomeWidth = 90, metronomeHeight = 240, metronomeYOffset = 100;

	metronome->setSize(metronomeWidth, metronomeHeight);
	metronome->setTopLeftPosition((positionRowCount * metronomeWidth), metronomeYOffset);
	addAndMakeVisible(metronome);
}

// ----- UI Listeners -----

void MetronomeManager::sliderValueChanged(Slider *slider)
{
	if (slider == &bpmSlider)
	{
		stopTimer();

		bpm = (int)slider->getValue();
		setAllMetronomeBpms();
		startTimer((60000/bpm));
	}
}