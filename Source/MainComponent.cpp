#include "MainComponent.h"

MainContentComponent::MainContentComponent()
{
    setSize(520, 400);

	titleText.setSize(300,100);
	titleText.setTopLeftPosition(90,0);
	titleText.setFont(Font(35.0f));
	titleText.setText("Metronomer", dontSendNotification);
	addAndMakeVisible(titleText);

	metronomer.setSize(600, 400);
	addAndMakeVisible(metronomer);
}

MainContentComponent::~MainContentComponent()
{
}
 
void MainContentComponent::paint(Graphics& g)
{
	g.fillAll(Colours::azure);

    g.setFont(Font(16.0f));
	g.setColour(Colours::blueviolet);
}

void MainContentComponent::resized()
{
}
