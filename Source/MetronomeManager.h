#pragma once
#include "Metronome.h"

///
/// Responsible for ensuring that all metronomes are correctly positioned on the UI, correctly synced and playing at the correct BPM.
///
class MetronomeManager : public Component, Slider::Listener, Timer, Thread
{
public:
	MetronomeManager(void);
	~MetronomeManager(void);
	
private:

	// Internal state
	int bpm;

	// Metronomes 
	Metronome kickDrum, snareDrum, tomDrum, highHat;

	// UI 
	Label bpmSliderLabel;
	Slider bpmSlider;

	// Metronome logic

	///
	/// Sets the BPM for all metronomes.
	///
	void setAllMetronomeBpms();

	///
	/// Syncs all of the metronomes, triggering any metronomes that are currently in the Syncing state to enter the Playing state.
	///
	void syncAllMetronomes();

	///
	/// Master timer that continuously resyncs the metronomes at the speed of the BPM.
	///
	void timerCallback() override;

	// Threading
	void run();


	// UI

	///
	/// Initialises a metronome UI, managing it's position and size.
	///
	void initialiseMetronomeUI(Metronome* metronome, int yOffset);

	// UI Listener
	void sliderValueChanged(Slider *slider);
};

