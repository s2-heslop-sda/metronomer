#pragma once
#include "JuceHeader.h"
#include "AudioFilePlayer.h"

///
/// Simple Metronome, complete with Start and Pause buttons and volume control.
///
class Metronome : public Component, Button::Listener, Label::Listener, Timer, Slider::Listener
{
public:
	Metronome(String audioFileName);
	~Metronome(void);

	///
	/// Controls the speed at which the Metronome plays.
	///
	void setBpm(int newBpm);

	///
	/// Allows for MetronomeManager to start the Metronome, provided that the Metronome is currently attempting to sync.
	///
	void syncTimerStart();

private:

	enum State
	{
		Syncing,
		Playing,
		Stopped
	};

	// Internal state of the metronome.
	int bpm, denominator;
	float amplitude;
	State state;

	// Internal Audio player that produes the audio for the metronome.
	AudioFilePlayer audioFilePlayer;

	// UI
	Label metronomeNameLabel, denominatorLabel;
	ImageButton playButton, pauseButton;
	Slider volumeSlider;
	int xStartingPosition, yStartingPosition, padding, buttonDimension;

	// Metronome logic
	int metronomeInterval();
	void setDenominator();
	void timerCallback() override;

	// UI Initialisation
	void initialiseMetronomeNameLabel(String audioFileName);
	void initialiseDenominatorLabel();
	void initialiseButton(ImageButton* button, String buttonType, int positionInRow);
	void initialiseVolumeSlider();
	
	// UI Listeners
	void buttonClicked(Button* clickedButton) override;
	void labelTextChanged(Label *changedLabel) override;
	void sliderValueChanged(Slider *slider);
};

